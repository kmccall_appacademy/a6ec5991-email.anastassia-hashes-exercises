#Anastassia Bobokalonova
#April 4, 2017

# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words_lengths = {}
  words = str.split
  words.each do |word|
    words_lengths[word] = word.length
  end
  words_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|key, int| int}.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |item, new_amount|
    older[item] = new_amount
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  ch_count = {}
  letters = word.chars.uniq
  letters.each do |ch|
    ch_count[ch] = word.count(ch)
  end
  ch_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = {}
  arr.each do |el|
    uniq_hash[el] = true
  end
  uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parity_frequency = {even: 0, odd: 0}
  numbers.each do |num|
    if num.even?
      parity_frequency[:even] += 1
    else
      parity_frequency[:odd] += 1
    end
  end
  parity_frequency
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  vowels = ["a", "e", "i", "o", "u"]
  string.each_char do |ch|
    vowel_count[ch] += 1 if vowels.include?(ch)
  end

  vowel_count.sort_by {|ch, count| count}.last.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  names = students.select {|name, month| month > 6}.keys
  combinations = []
  names.each_index do |idx1|
    (idx1 + 1...names.length).each do |idx2|
      combinations << [names[idx1], names[idx2]]
    end
  end
  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species_count = Hash.new(0)
  specimens.each do |species|
    species_count[species] += 1
  end
  number_of_species = species_count.length
  smallest_population_size = species_count.values.min
  largest_population_size = species_count.values.max

  number_of_species ** 2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)
  vandalized_count.each do |letter, count|
    return false if normal_count[letter] < count
  end
  true
end

def character_count(str)
  count = Hash.new(0)
  str.each_char do |ch|
    count[ch.downcase] += 1
  end
  count
end
